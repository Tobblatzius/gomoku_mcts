from copy import deepcopy
from collections import defaultdict
# from pathos import multiprocessing as mp
import multiprocessing as mp2
import os
import numpy as np
import random
import pickle
import argparse
import re
import sys
import importlib.util


import torch
import torch.nn as nn
import torch.nn.functional as F

from module import TrainableModel

BOARD_SIZE = 7
NR_SQUARES = BOARD_SIZE ** 2
IN_ROW = 5

dir_path = os.path.dirname(os.path.realpath(__file__))

spec = importlib.util.spec_from_file_location(
    "pathos", "../pathos/__init__.py")
mp = importlib.util.module_from_spec(spec)
spec.loader.exec_module(mp)
mp = mp.multiprocessing

""" CNN representing estimated value for each board state.
"""


class Net(TrainableModel):

    def __init__(self):

        super(TrainableModel, self).__init__()
        self.conv1 = nn.Conv2d(1, 32, kernel_size=(3, 3), padding=(1, 1))
        self.conv2 = nn.Conv2d(32, 32, kernel_size=(3, 3), padding=(1, 1))
        self.layer1 = nn.Linear(32, 32)
        self.layer2 = nn.Linear(32, 1)

    def loss(self, data, data_pred):
        # adding [0] to fix bug in Gomoku
        Y_pred = data_pred["target"][0].float()
        Y_target = data["target"].float()
        #print ((Y_pred, Y_target))

        return (F.mse_loss(Y_pred, Y_target))

    def forward(self, x):
        x = x['input'].float()
        x = x.view(-1, 1, BOARD_SIZE, BOARD_SIZE)
        x = F.relu(self.conv1(x))
        x = F.relu(self.conv2(x))
        x = F.max_pool2d(x, (3, 3))[:, :, 0, 0]
        #x = F.dropout(x, p=0.5, training=self.training)

        x = self.layer1(x)
        x = self.layer2(F.relu(x))

        return {'target': x}

        # x = F.max_pool2d(x, (4, 4))[:, :, 0, 0]
        # x = F.dropout(x, p=0.2, training=self.training)

        # x = self.layer1(x)
        # x = self.layer2(F.tanh(x))

        # return {'target': x}


class Node():
    """
    Class representing nodes in the tree
    Parameters
    ----------
        value
            Conten of the node
        parent : Node, optional
            The parent of the node (the deafult is None)
        children : list of Node, optional
            List of nodes of all children to the node (the deafult is None)
    Attributes
    ----------
        value
            Conten of the node
        parent : None or Node
            The parent of the node
        children : None or list of Node
            List of nodes of all children to the node
    """

    def __init__(self, value, parent=None, children=None):
        super().__init__()
        self.value = value
        self.parent = parent
        self.children = children

    def set_new_children(self, children_values, overwrite=True):
        """Edits the children of a node
        Parameters
        ----------
            children_values : list of Node
                List of nodes of all children to the node
            overwrite : bool, optional
                Indicates if operation should overwrite existing children (the default is true)
        """
        children = [] if overwrite or not self.children else self.children
        for value in children_values:
            children.append(Node(value, parent=self))
        self.children = children

# The game and its general rules


class GameOverException(Exception):
    """
    Exception calss for flaging game over
    Parameters
    ----------
        message : str
            Message to be displayed 
    Attributes
    ----------
        message : str
            Displaye message
    """

    def __init__(self, message):
        # Call the base class constructor with the parameters it needs
        super().__init__(message)

# The current state of a game


class Gomoku_GameState:
    """
    Class for representing a state of a game of Gomoku
    Note
    ----
        Class should be treated as imutable 
    Parameters
    ----------
        player_turn : int, optional
            Player number of the player who is about to play (default is 1)
        state : numpy.ndarray, optional
            State of the board as a 3 by 3 matrix (deafult is np.zeros((3, 3), int))
    Attributes
    ----------
        player_turn : int
            Player number of the player who is about to play 
        state : numpy.ndarray
            State of the board
    """

    def __init__(self, player_turn=1, state=np.zeros((BOARD_SIZE, BOARD_SIZE), int)):
        super().__init__()
        self._player_turn = player_turn
        self._state = deepcopy(state)
        # for i in range(5):
        #     self._state[(8-i,i)] = 1
        self.hash_matrix = self.get_prime_matrix(n=BOARD_SIZE)

    @property
    def player_turn(self):
        return self._player_turn

    @property
    def state(self):
        return deepcopy(self._state)

    def get_prime_matrix(self, n):
        def isPrime(n):
            return re.match(r'^1?$|^(11+?)\1+$', '1' * n) == None
        M = 100            # upper-bound of search space
        primes = list()           # result list
        while len(primes) <= n*n+1:
            primes += filter(isPrime, range(M - 100, M))
            M += 100

        prime_matrix = []
        row = []
        for idx in range(n*n):
            # new row of the board
            if idx % n == 0 and idx != 0:
                prime_matrix.append(row)
                row = []
            # +1 to skip the prime number 2
            row.append(primes[idx+1])

        prime_matrix.append(row)
        return np.array(prime_matrix)

    def __hash__(self):
        """Hash function. Will return the same hash for equivalent states.
        Returns:
            int -- hash of the game state
        """
        # board_value = self.get_prime_matrix(n=3)
        # board_value = np.array([[3, 5, 7], [11, 13, 17], [19, 23, 29]])

        next_state = self.state
        best_hash = int(np.prod(np.power(self.hash_matrix, next_state)))

        # 8 possible symmetries (flip along 2 vertical, 2 horizontal and 4 diagonal)
        for i in range(8):
            if i == 3:
                next_state = np.fliplr(next_state)
            next_state = np.rot90(next_state, 1)
            next_hash = int(np.prod(np.power(self.hash_matrix, next_state)))
            if next_hash < best_hash:
                best_hash = next_hash

        # return the "base" state, corresponding to the symmetric state with lowest hash. Also need to consider which player that will do the next move.
        return best_hash * self.player_turn

    def get_unique_childstates(self):
        """Gets all possible and unique gamestates for the next move.
        Returns:
            list of Gomoku_GameState -- All possible and unique gamestates for the next move.
        """
        if self.is_win(1) or self.is_win(2) or self.is_tie():
            return []

        hashes = []
        childstates = []
        idxs = np.where(self.state == 0)
        idxs = list(zip(*idxs))
        np.random.shuffle(idxs)

        for idx in idxs:
            new_state = self.state
            new_state[idx] = self.player_turn
            new_state = type(self)(self.next_player(), new_state)
            new_hash = hash(new_state)

            # if symmetric childstate already is found
            if new_hash in hashes:
                continue

            hashes.append(new_hash)
            childstates.append(new_state)
        # list of base state childstates
        return childstates

    def next_player(self):
        return 1 if self.player_turn == 2 else 2

    def __setitem__(self, key, value):
        self.state.__setitem__(key, value)

    def __getitem__(self, key):
        return self.state.__getitem__(key)

    def is_win(self, player):
        """Checks if the game state is a winning state for player
        Arguments:
            player {int} -- Player number to check winning state for
        Returns:
            bool -- True if it is a winning state else False
        """

        def check_array(arr, player):
            'Check if we have 5 player item in a row.'
            nr_in_row = 0
            max_nr_in_row = 0
            for a in arr:
                if a == player:
                    nr_in_row += 1
                else:
                    max_nr_in_row = max(max_nr_in_row, nr_in_row)
                    nr_in_row = 0
            max_nr_in_row = max(max_nr_in_row, nr_in_row)
            if max_nr_in_row >= IN_ROW:
                return True
            return False

        # Check horizontal and vertical
        for idx in range(5):
            row = self[idx, :]
            col = self[:, idx]

            if check_array(row, player):
                return True
            if check_array(col, player):
                return True
        # Check diagonals in all 5x5 submatrices
        for r_idx in range(IN_ROW):
            for c_idx in range(IN_ROW):
                # Upper left to down right
                diagonal_1 = np.diagonal(
                    self.state[r_idx:r_idx+IN_ROW, c_idx:c_idx+IN_ROW])
                # Lower left to up right
                diagonal_2 = np.diagonal(
                    np.flipud(self.state[r_idx:r_idx+IN_ROW, c_idx:c_idx+IN_ROW]))

                if check_array(diagonal_1, player) \
                        or check_array(diagonal_2, player):
                    return True  # Player wins
        return False

    def is_tie(self):
        """Checks if no more moves can be made.
        Returns:
            bool -- True if board is full, else False
        """
        if len(np.where(self.state == 0)[0]) == 0:
            return True
        return False

    def __str__(self):
        out = f"Player turn: {self.player_turn}\n"
        out += str(self.state)
        return out

    def __repr__(self):
        return self.__str__()


class Gomoku():
    """ Class representing a game of Gomoku
    Parameters
    ----------
        game_state : Gomoku_GameState, optional
            Current state of the game (default is a new Gomoku_GameState())
        verbose : bool, optional
            Wether or not to announce the state of the game after each move (default is True)
    Attributes
    ----------
         game_state : Gomoku_GameState
            Current state of the game.
        verbose : bool
            Wether or not to announce the state of the game after each move.
    """

    def __init__(self, game_state=Gomoku_GameState(), verbose=True):
        super().__init__()
        self.game_state = deepcopy(game_state)
        self.verbose = verbose

    def make_move(self, move):
        """Updates the game with a new move
        Arguments:
            move {tuple of int} -- tuple of int reprecenting index for where the move was made.
        """
        if not self.valid_move(move):
            print('AI played an non valid_move, quiting the game.')
            raise GameOverException

        new_state = self.game_state.state
        new_state[move] = self.game_state.player_turn
        new_state = type(self.game_state)(
            self.game_state.next_player(), new_state)
        # TODO: check legal move
        self.game_state = new_state

        if self.verbose:
            if new_state.is_win(self.game_state.next_player()):
                print(f"Player {self.game_state.next_player()} wins!")
            elif new_state.is_tie():
                print("It's a tie...")
            print(self)

    def valid_move(self, move):
        # Empty spot?
        if self.game_state.state[move] != 0:
            print('This spot is already occupied, try again...')
            return False
        # Move outside of board?
        if move[0] < 0 \
                or move[0] > BOARD_SIZE\
                or move[1] < 0\
                or move[1] > BOARD_SIZE:
            print('Correctly format but outside of board, try again...')
            return False
        return True

    def __str__(self):
        out = f"Gomoku game\n"
        out += f"Player turn: {self.game_state.player_turn}\n"
        out += str(self.game_state.state)
        return out

    def __repr__(self):
        return self.__str__()


class Gomoku_AI():
    """ Class for ai that learns to play and plays Gomoku. Uses montecarlo tree search using upper confidence bounds.
    Parameters
    ----------
        player : int, optional
            What player the ai should plays as (default is 1)
        game_knowledge : dict, optional
            Storage for the "knowledge" that the ai gains during play (the default is defaultdict(lambda: {"Q": 0, "N": 0, "A":[0,0,0]}) )
        game : Gomoku, optional
            Instance of the game that the ai will play on (deafult is a new Gomoku instance)
        T : int, optional
            Number of simulations to run before choosing what to do (default is 10)
        c : int, optional
            Exploration constant (default is 1)
    Attributes
    ----------
        player : int
            What player the ai plays as.
        game_knowledge : dict
            The "knowledge" that the ai gained during play.
        game : Gomoku
            Instance of the game that the ai plays on.
        T : int
            Number of simulations to run before choosing what to do.
        c : int
            Exploration constant
    """

    def __init__(self, player=1, game_knowledge=defaultdict(lambda: {"Q": 0, "N": 0, "A": [0, 0, 0]}), game=Gomoku(), T=10, c=1, model=Net()):
        super().__init__()
        self.player = player
        self.game_knowledge = game_knowledge
        self.game = game
        self.T = T
        self.c = c
        self.model = model

    def pickle_knowledge(self, path="knowledge"):
        with open(path+'.pickle', 'wb') as fp:
            pickle.dump(dict(self.game_knowledge), fp,
                        protocol=pickle.HIGHEST_PROTOCOL)

    def load_from_pickle(self, path="knowledge"):
        with open(path+'.pickle', 'rb') as fp:
            self.game_knowledge = defaultdict(
                lambda: {"Q": 0, "N": 0, "A": [0, 0, 0]}, pickle.load(fp))

    def play(self):
        """Tells the Ai to make a move.
        Raises:
            GameOverException: If play is run after game has ended.
        """
        new_state = self.monte_carlo_tree_search(self.game.game_state)
        idx = np.where(new_state.state - self.game.game_state.state != 0)
        try:
            move = list(zip(*idx))[0]
        except IndexError as e:
            e.with_traceback
            raise GameOverException("No moves to make")
        self.game.make_move(move)

    def monte_carlo_tree_search(self, game_state):
        """Base function for montecarlo tree search. Executes the four steps Selection, Expantion, Roll-out and Backpropagation T times, updating game_knowledge every time.
        Arguments:
            game_state {Gomoku_GameState} -- The state from where the tree search should start
        Returns:
            Gomoku_GameState -- The next move to execute
        """
        root = Node(game_state)
        root.set_new_children(root.value.get_unique_childstates())
        for _ in range(self.T+1):
            leaf, terminal_node = self.traverse(root)  # leaf = unvisited node
            # If the traversal did not lead us to terminal node, run simulations
            if not terminal_node:
                simulation_result, sim_leaf = self.rollout(leaf)
                self.backpropagate(sim_leaf, simulation_result)

            return self.get_best_child(root).value

    def traverse(self, node):
        """
        Traverse down the tree until we reach a node where not all children are visited.
        Arguments:
            node -- Node
        Returns:
            node -- Node
                next node to go to in traverse
        """
        while self.fully_expanded(node):
            # in case no children are present / node is terminal
            if not node.children:
                terminal_node = True
                return node, terminal_node
            node = self.best_uct(node)
        next_node = self.pick_unvisited(node.children)
        if next_node == None:
            terminal_node = True
            return node, terminal_node
        terminal_node = False
        return next_node, terminal_node

    def rollout(self, node):
        """Fast simulation of the game from node to first terminal node
        Arguments:
            node {Node} -- Node from where to start the rollout
        Returns:
            int -- 1 if rollout ends in player 1 winning node, -1 if player 2 winning node and 0 if tied
        """
        while not self.terminal(node):
            node = self.rollout_policy(node)
        return self.result(node)

    def rollout_policy(self, node):
        """
        Policy for how to select nodes in simulation part. We use uniform random selection.
        Arguments:
            node -- Node
        Returns:
            node -- Node
                next node in simulation
        """
        if node.children == None:
            node.set_new_children(node.value.get_unique_childstates())
        return self.pick_random(node.children)

    def backpropagate(self, node, result):
        """
        Recursive propagation of result
        Q: Q-score
        N: number of visits
        A: list, [nrof ties, nr of player1 win, nr of player 2 win]
        Arguments:
            node -- Node
            result -- int
        """
        # fit CNN with state node.value with result from simulation as target
        dataset = [{'input': node.value.state, 'target': result}]
        self.model.fit(dataset, batch_size=1, verbose=False)

        if node.parent == None:
            return

        self.backpropagate(node.parent, result)

    def get_best_child(self, root):
        """
        Make the best move by the end of MCTS. Picks the most visisted child of the root.
        Arguments:
            root -- Node
        Returns:
            best_child -- Node
        """
        best_child = root
        n_max = -1
        for child in root.children:
            child_hash = hash(child.value)
            if self.game_knowledge[child_hash]['N'] > n_max:
                best_child = child
                n_max = self.game_knowledge[child_hash]['N']
        return best_child

    def best_uct(self, node):
        """
        In traversal of MCTS, traverse the tree down through the nodes with 
        highest upper confidence bound.
        Arguments:
            node -- Node
        Returns:
            best_child -- Node
        Note: 
            We had a minor bug here since we sometimes ended up in this function even though we had unvisited children in this layer of the tree. Therefore we manually set uct to inf if we had a node here which was not yet visited. This bug is probably fixed, but just in case...
        """
        c = self.c  # Exploration constant
        uct_max = -np.inf
        best_child = None
        N = self.game_knowledge[node_hash]['N']
        for child in node.children:
            child_hash = hash(child.value)

            q = self.network_value(child)
            n = self.game_knowledge[child_hash]['N']

            if n == 0:
                # There was a bug here, this might be unnecessary but would be sad if it crashes
                uct_max = np.inf
                best_child = child
                break

            uct = q + c*np.sqrt(np.log(N)/n)

            if uct > uct_max:
                uct_max = uct
                best_child = child

        if best_child == None:
            best_child = self.pick_random(node.children)

        return best_child

    def fully_expanded(self, node):
        """
        Arguments:
            node -- Node
                node to be checked whether fully expanded (all children visited)
        Returns:
            bool -- 
                True if all children has been visited
                False else
        """
        if node.children == None:
            node.set_new_children(node.value.get_unique_childstates())
        for child in node.children:
            if self.game_knowledge[hash(child.value)]["N"] == 0:
                return False
        return True

    def pick_unvisited(self, nodes):
        """
        Arguments:
            nodes -- list of Nodes
                nodes are children of a not fully expanded node,
                find the unvisited ones among these children and return
                one of them using uniform random sampling.
        Returns:
            node -- Node
        """

        unvisited = []
        for node in nodes:
            # if node not in gameknowledge
            if self.game_knowledge[hash(node.value)]["N"] == 0:
                unvisited.append(node)
        if unvisited == []:
            return None
        return self.pick_random(unvisited)

    def pick_random(self, nodes):
        """
        uniform random selection of a Node
        Arguments:
            nodes -- list of Nodes
        Returns:
            node -- Node       
        """
        return random.choice(nodes)

    def terminal(self, node):
        """
        Check if node is terminal (win, loss or tie)
        Arguments:
            node -- Node
        Returns:
            bool
                True if win, loss or tie
        """
        return node.value.is_win(1) or node.value.is_win(2) or node.value.is_tie()

    def network_value(self, node):
        dataset = [{'input': node.value.state, 'target': None}]
        return self.model.predict(dataset).mean()

    def result(self, node):
        """To update Q-values, was terminal node win, loss or tie for the player?
        Arguments:
            node -- Node
                a terminal node
        Returns:
            +1 if win
            -1 if lost
             0 if tie
        """

        # messing with it to obtain a float type array (pytorch wants it)
        res = 1 if node.value.is_win(1) else -1 if node.value.is_win(2) else 0
        return res, node


class Gomoku_HumanPlayer():
    def __init__(self, player=1, game=Gomoku()):
        super().__init__()
        self.player = player
        self.game = game

    def play(self):
        while True:
            move = input("Enter your move as tuple (row, col): ")
            move = re.sub("[^0-9]", "", move)
            if len(move) != 2:
                print("Incorrect board-coordinate, try again...")
                continue
            move = tuple([int(x) for x in move])

            if self.game.valid_move(move):
                break
        self.game.make_move(move)


def train_mp(n, ai1, ai2):
    '''
    Train the AI using multiprocessing
    '''
    game = Gomoku(verbose=False)
    ai1.game = game
    ai2.game = game

    while True:
        ai1.play()
        ai2.play()
    # when game is over update the score
    if game.game_state.is_win(1):
        return 1
    elif game.game_state.is_win(2):
        return 2
    elif game.game_state.is_tie():
        return 3


def slice_data(data, nprocs):
    aver, res = divmod(len(data), nprocs)
    nums = []
    for proc in range(nprocs):
        if proc < res:
            nums.append(aver + 1)
        else:
            nums.append(aver)
    count = 0
    slices = []
    for proc in range(nprocs):
        slices.append(data[count: count+nums[proc]])
        count += nums[proc]
    return slices


def main():

    parser = argparse.ArgumentParser(description="Gomoku AI!")
    parser.add_argument(
        '-l', '--load', help='Should we train or use existing model', action='store_true')
    parser.add_argument(
        '-s', '--save', help='Should we save the trained model when done?', action='store_true')
    parser.add_argument(
        '-v', '--verbose_', help='Print the board of the game and stats?', action='store_true')
    parser.add_argument(
        '-mp', '--multiprocessing', help='Train with parallelization over all available cores?', action='store_true')
    parser.add_argument(
        '-p', '--play', help='Do you want to play against the AI?', action='store_true')
    parser.add_argument(
        '-ln', '--loadname', help='Where to load the model?', default='singleCPUtrain')
    parser.add_argument(
        '-sn', '--savename', help='Where to save the model?', default='default_savename')
    args = parser.parse_args()

    # Try to read stored game_knowledge
    if args.load:
        try:
            with open(f'{args.loadname}.pickle', 'rb') as fp:
                gk = defaultdict(lambda: {"Q": 0, "N": 0, "A": [
                    0, 0, 0]}, pickle.load(fp))
            model = torch.load(f'{args.loadname}Torchmodel')
            model.compile(torch.optim.Adadelta, lr=0.3)

        except Exception as e:
            print(str(e))
            print(
                f"Could not open model from {args.loadname}.pickle. Runs from blank knowledge")
            gk = defaultdict(lambda: {"Q": 0, "N": 0, "A": [0, 0, 0]})
            model = torch.load(f'{args.loadname}Torchmodel')
            model.compile(torch.optim.Adadelta, lr=0.3)

    else:
        gk = defaultdict(lambda: {"Q": 0, "N": 0, "A": [0, 0, 0]})
        model = Net()
        model.compile(torch.optim.Adadelta, lr=0.3)

    if not args.play:
        if args.load:
            # Create two AIs charing the knowledge of 1000 self plays with 1000 simulations per move, making no new simulations themselves (for speed)
            ai1 = Gomoku_AI(player=1, T=0, game_knowledge=gk, model=model)
            ai2 = Gomoku_AI(player=2, T=0, game_knowledge=gk, model=model)
            # or create two ais with separate knowledge, 1 making 1 simulations per turn and 2 making 2. (to be able to see the learning)
        else:

            ai1 = Gomoku_AI(player=1, T=1, c=1, model=model)
            ai2 = Gomoku_AI(player=2, T=1, c=1, model=model)

        # Create multiprocessing pool
        if args.multiprocessing:
            nbr_rounds = 8
            # nprocs = mp.cpu_count()
            nprocs = 8
            pool = mp2.Pool(processes=nprocs)
            mp2.set_start_method('spawn', force=True)
            # _ = pool.starmap(train_mp, [(n, ai1, ai2) for n in range(nbr_rounds)])
            inp_lists = slice_data(range(nbr_rounds), nprocs)
            multi_result = [pool.apply_async(
                train_mp, (inp, ai1, ai2)) for inp in inp_lists]
            result = [x for p in multi_result for x in p.get()]

            ai2.pickle_knowledge(path=args.savename)

        else:
            wins1 = 0
            wins2 = 0
            ties = 0
            nbr_rounds = 100
            # Play nbr_rounds rounds
            for i in range(nbr_rounds):
                print(f'Round number {i+1}')
                game = Gomoku(verbose=args.verbose_)
                if i == nbr_rounds-1:  # show the complete last game
                    game = Gomoku(verbose=args.verbose_)

                ai1.game = game
                ai2.game = game
                try:
                    while True:
                        ai1.play()
                        ai2.play()
                except GameOverException:
                    # when game is over update the score
                    if game.game_state.is_win(1):
                        wins1 += 1
                    elif game.game_state.is_win(2):
                        wins2 += 1
                    elif game.game_state.is_tie():
                        ties += 1

                    # Every 10% of nbr_games save game_knowledge and print stats plus latest game
                    if(i % (nbr_rounds//10) == (nbr_rounds//10)-1):

                        if args.save:
                            ai2.pickle_knowledge(path=args.savename)

                        if args.verbose_:
                            print()
                            print(f"{i // (nbr_rounds//10) + 1}0%")
                            print(
                                f"wins 1: {wins1}\nwins 2: {wins2}\nties: {ties}")
                            print(f"finishing game state of game {i+1}")
                            print(game)
    elif args.play:
        game = Gomoku(verbose=True)
        human = Gomoku_HumanPlayer(player=2)
        if args.load:
            ai = Gomoku_AI(player=1, T=0, game_knowledge=gk, model=model)
        else:
            ai = Gomoku_AI(player=1, T=1, c=1)

        ai.game = game
        human.game = game

        try:
            while True:
                ai.play()
                human.play()
        except GameOverException:
            # when game is over update the score
            if game.game_state.is_win(1):
                print('AI player won!')
            elif game.game_state.is_win(2):
                print('Congratulations, you beat the AI!')
            elif game.game_state.is_tie():
                print('What an even game, there is a tie!')


if __name__ == "__main__":
    main()
