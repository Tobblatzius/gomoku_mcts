# Gomoku_MCTS

Gomoku_MCTS is the complete and working Monte Carlo Search Tree implementation on a smaller variant of Gomoku. The parameters of the game may be set inside the file.

Gomoku_CNN is a not yet completed version which further utilzies CNNs for feature representation of the game.

Two trained models are included:
'5by5_16proc_100games_100depth.pickle' is a pretrained model for a 5x5 board.
'3by3_4proc_100games_1sim.pickle' is a pretrained model for a 3x3 board.

The models take around 30 seconds to load.

If you want to see when the pretrained model plays against a random playing agent in 100 games, enter:  
<code>python3 Gomoku_MCTS.py -v -l -ln 5by5_16proc_100games_100depth</code>

If you want to play against the pretrained model, enter:  
<code>python3 Gomoku_MCTS.py -v -p -l -ln 5by5_16proc_100games_100depth</code>

If you want to train a model and save ut using multiprocessing, change the parameters in the file and enter:  
<code>python3 Gomoku_MCTS.py -mp -s -sn SAVENAME</code>

If you want to try out the 3x3 version you do similarily but change the board size and in_row parameter in the file.

